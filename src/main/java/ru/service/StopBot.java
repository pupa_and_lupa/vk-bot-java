package ru.service;
/**
 * Корректное завершение работы юота.
  * @author А. Мягков
  * @version 1.0
 */
public class StopBot {
    /**
     * Вызов системной функции остановки программы.
     */
void stopBot()
{
    System.exit(2);
}
}
