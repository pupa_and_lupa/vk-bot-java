package ru.service;

import com.petersamokhin.bots.sdk.callbacks.messages.OnMessageCallback;
import com.petersamokhin.bots.sdk.clients.Group;
import com.petersamokhin.bots.sdk.objects.Message;
import com.petersamokhin.bots.sdk.utils.vkapi.calls.CallAsync;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


// Команда разбан только для админа. Для сначала сделать проверку забаненных
/**
 * Main-класс прораммы, запуск бота, обработка команд и сообщений.
 * @author А. Мягков
 * @version 1.0
 */
public class Main {
    /**
     * Основная функция.
     * @param args - ключи командной строки.
     */
    public  static void main (String[] args)
    {
        final  String access_token = "71cff2f24f0bdbd501cc3d286a9c4b2b31a260b48099e1ff8be268ac53bff31ebb12211a8629d1c9aa0c7";

        Authors authors = new Authors();
        Quest quests = new Quest();
        Banned ban = new Banned();
        Sources sources = new Sources();
        Group group = new Group(172390739, access_token);

        /*    group.onMessage(message -> {System.out.println(message);
       });*/

        /** Устанавливаем онлайн сообщества */
        JSONObject on_off = new JSONObject();
        on_off.put("group_id", "172390739");
        CallAsync online = new CallAsync("groups.enableOnline", on_off, response -> System.out.println("Онлайн сообщества включен."));
        group.api().execute(online);

        /** Блок команд бота; */
        /**
         * Команда остановки.
         */
        group.onCommand(new String[]{"!стоп", "!stop", "!end", "/end", "/стоп", "/stop"}, message ->

        {

            if (authors.getAdm(message.authorId()) > 1) {
                CallAsync offline = new CallAsync("groups.disableOnline", on_off, response -> System.out.println("Онлайн сообщества выключен."));
                group.api().execute(offline);
                new Save().saveAuthors(authors);
                new Save().saveQuest(quests.questionBase);
                new Save().saveBan(ban);
                new Message()
                        .from(group)
                        .to(message.authorId())
                        .text("Слушаю и повинуюсь, хозяин.")
                        .send();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                }
                new StopBot().stopBot();
            }
            else
            {
                new Message()
                        .from(group)
                        .to(message.authorId())
                        .text("Посторонним доступ запрещен.")
                        .send();
            }

        });
        /**
         * Команда помощи.
         */
        group.onCommand("!помощь", message ->
        {
            String response;
            response = "Команды:\n!сначала - пройти квест сначала.";
            if (authors.getAdm(message.authorId())> 0)
            {
                response = response + "\n!разбан id - разбанить пользователя.\n!бан - посмотреть забаненных пользователей.";
            }
            if (authors.getAdm(message.authorId())>1)
            {
                response = response + "\n!стоп - остановить бота.\n!разбан всех - разбанить всех пользователей.\n!назначить id admlvl - назначить пользователя администратором бота.";
            }
            new Message()
                    .from(group)
                    .to(message.authorId())
                    .text(response)
                    .send();
        });
        /**
         * Команда вывода информации о банах.
         */
        group.onCommand("!бан", message ->
        {
            if (authors.getAdm(message.authorId())>0) {
                if (!ban.isEmpty()) {
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Список забаненных пользователей:\n")
                            .send();
                    for (Map.Entry<Long, Date> item : ban.banList.entrySet()) {
                        JSONObject user = new JSONObject();
                        user.put("user_ids", item.getKey());
                        CallAsync users = new CallAsync("users.get", user, resp -> {
                            String[] tmp = resp.toString().split("\"");
                            String name1 = tmp[9] + " " + tmp[3];
                            new Message()
                                    .from(group)
                                    .to(message.authorId())
                                    .text("[id" + item.getKey() + "|" + name1 + "] (id - " + item.getKey() + ")\n")
                                    .send();

                        });
                        group.api().execute(users);
                    }

                } else {
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Список забаненных пользователей пуст.")
                            .send();
                }
            }
            else
                {
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Error. Page not found.")
                            .send();


                }
        });
        /**
         * Команда разбана всех пользователей.
         */
        group.onCommand("!разбан всех", message -> {
            if (authors.getAdm(message.authorId())>1)
            {
                if(!ban.isEmpty())
                {
                    LinkedList<Long> deleteBan = new LinkedList<>();
                    for (Map.Entry<Long, Date> item: ban.banList.entrySet())
                    {
                        deleteBan.push(item.getKey());
                    }
                    while(!deleteBan.isEmpty())
                    {
                        long tmp = deleteBan.pop();
                        ban.deleteBan(tmp);
                        JSONObject user = new JSONObject();
                        user.put("user_ids", message.authorId().toString());
                        CallAsync users = new CallAsync("users.get", user, resp ->{
                            String[] tmp1 = resp.toString().split("\"");
                            String name1 = tmp1[9] + " " + tmp1[3];

                            new Message()
                                    .from(group)
                                    .to(Integer.parseInt(Long.toString(tmp)))
                                    .text("Вы были разбанены администратором [id" + message.authorId()+ "|" + name1 + "]. Не допускайте ошибок. Администрация не может Вас каждый раз разбанивать.")
                                    .send();
                        });
                        group.api().execute(users);


                    }
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Все пользователи были разбанены.")
                            .send();
                }
                else
                {
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Нет забаненных пользователей.")
                            .send();
                }
            }
            else
            {
                new Message()
                        .from(group)
                        .to(message.authorId())
                        .text("Нет доступа.")
                        .send();
            }
        });
        /**
         * Команда для начала теста сначала.
         */
        group.onCommand("!сначала", message ->
        {
            if (!ban.getBanned(message.authorId())) {
                new Message()
                        .from(group)
                        .to(message.authorId())

                        .text("Пройди этот квест еще раз.")
                        .send();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                authors.putAuthor(message.authorId(), 0, 3, authors.getAdm(message.authorId()), authors.getSticker(message.authorId()));

            }
            else
                new Message()
                        .from(group)
                        .to(message.authorId())

                        .text("Ты забанен, поэтому не можешь пройти квест сначала.")
                        .send();
        });





        /**
         * Реакции на все остальные сообщения и команды.
         */
        group.onMessage(message ->
        {
            group.enableTyping(true);
            long authorId = message.authorId();
            int param;
            if (authors.getAuthor(authorId)) {
                param = authors.getParam(authorId);
            } else {
                param = 0;
                authors.putAuthor(authorId, param, 3, 0, 0);
            }

            if ((message.getText().matches("!разбан (.*)"))||(message.getText().matches("/разбан (.*)")))
            {
                if((authors.getAuthor(message.authorId()))&&(authors.getAdm(message.authorId())>0))
                {
                    long id = Long.parseLong(message.getText().split(" ")[1]);
                    if ((authors.getAuthor(id))&&(ban.getBanned(id) == true)) {

                        ban.deleteBan(id);

                        if (message.authorId() == id){
                            new Message()
                                    .from(group)
                                    .to(message.authorId())
                                    .text("Прости, хозяин, что забанил тебя. Ты уже разбанен. Можешь проходить квест снова.")
                                    .send();
                        }
                        else
                        {
                            JSONObject user = new JSONObject();
                            user.put("user_ids", message.authorId().toString() + ", " + Long.toString(id));
                            CallAsync users = new CallAsync("users.get", user, resp ->{
                                String[] tmp = resp.toString().split("\"");
                                String name1 = tmp[9] + " " + tmp[3];
                                String name2 = tmp[19] + " " + tmp[13];

                                new Message()
                                        .from(group)
                                        .to(message.authorId())
                                        .text("Пользователь [id" + id + "|"+ name2 + "] разбанен.")
                                        .send();

                                new Message()
                                        .from(group)
                                        .to(Integer.parseInt(Long.toString(id)))
                                        .text("Вы были разбанены администратором [id" + message.authorId()+ "|" + name1 + "]. Не допускайте ошибок. Администрация не может Вас каждый раз разбанивать.")
                                        .send();
                            });
                            group.api().execute(users);
                        }
                    }

                    else {new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Пользователь с ID " + id + " не найден или не забанен!")
                            .send();
                    }
                }
                else
                {
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("А ну не трогай тут ничего.")
                            .send();
                }

            }

            else if(message.getText().matches("!назначить (.*)"))
            {
                if((authors.getAuthor(message.authorId()))&&(authors.getAdm(message.authorId())>1))
                {
                    final int maxlvl = 3;
                    final int minlvl = 0;
                    long id = Long.parseLong(message.getText().split(" ")[1]);
                    int lvl = Integer.parseInt(message.getText().split(" ")[2]);
                    if ((authors.getAuthor(id))&&(authors.getAdm(id) < authors.getAdm(message.authorId()))&&(authors.getAdm(message.authorId())>=(maxlvl - 1))&&(lvl >= minlvl)&&(lvl <= authors.getAdm(message.authorId()))) {
                        authors.putAuthor(id, authors.getParam(message.authorId()), authors.getHp(message.authorId()), lvl, authors.getSticker(message.authorId()));
                        if (lvl > 0) {
                            JSONObject user = new JSONObject();
                            user.put("user_ids", message.authorId().toString() + ", " + Long.toString(id));
                            CallAsync users = new CallAsync("users.get", user, resp ->{
                                String[] tmp = resp.toString().split("\"");
                                String name1 = tmp[9] + " " + tmp[3];
                                String name2 = tmp[19] + " " + tmp[13];

                                new Message()
                                        .from(group)
                                        .to(message.authorId())
                                        .text("Пользователь [id" + id + "|"+ name2 + "] назначен администратором " + lvl + " уровня.")
                                        .send();

                                new Message()
                                        .from(group)
                                        .to(Integer.parseInt(Long.toString(id)))
                                        .text("[id" + message.authorId()+ "|" + name1 + "] назначил тебя администратором " + lvl + " уровня.")
                                        .send();
                            });
                            group.api().execute(users);


                        }
                        else
                        {
                            JSONObject user = new JSONObject();
                            user.put("user_ids", message.authorId().toString() + ", " + Long.toString(id));
                            CallAsync users = new CallAsync("users.get", user, resp ->{
                                String[] tmp = resp.toString().split("\"");
                                String name1 = tmp[9] + " " + tmp[3];
                                String name2 = tmp[19] + " " + tmp[13];
                            new Message()
                                    .from(group)
                                    .to(message.authorId())
                                    .text("Пользователь [id" + id + "|" + name2 + "] снят с должности администратора.")
                                    .send();

                            new Message()
                                    .from(group)
                                    .to(Integer.parseInt(Long.toString(id)))
                                    .text("Увы, ты не оправдал моих надежд, поэтому я снимаю тебя с должности администратора, по просьбе разработчика [id" + message.authorId() + "|" + name1 + "].")
                                    .send();
                        });
                            group.api().execute(users);
                        }

                    }

                    else {new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Вы не можете назначить данного пользователя на данный уровень администратора(Пользователя нет, он уже администратор, у вас недостаточно прав, не верный уровень, нельзя назначать самого себя).")
                            .send();
                    }
                }
                else
                {
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("А ну не трогай тут ничего.")
                            .send();
                }

            }

            else if (message.getText().matches("!(.*)"))
            {
                new Message()
                        .from(group)
                        .to(message.authorId())
                        .text("Команда не найдена.")
                        .send();
            }

            else
            {
                if (ban.getBanned(message.authorId()))
                {
                    String text = new String();
                    if (new Date( ban.getTime(message.authorId()).getTime()- new Date().getTime()).getTime()/60000 < 1)
                    {
                        text = "меньше 1 минуты.";
                    }
                    else
                    {
                    text =  text + new Date(ban.getTime(message.authorId()).getTime()- new Date().getTime()).getTime()/60000 + " минут.";
                    }
                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text("Ты не прошел квест, я с тобой не разговариваю. (Осталось: " + text + ")")
                            .send();
                }
                else {
                    boolean flag = false;
                    ArrayList<String> res;

                    if(param == 8) {
                        res = quests.quest(authors.getParam(message.authorId()), message.toString(), message.authorId());
                    }
                    else if (param == 9)
                    {
                        res = quests.quest(authors.getParam(message.authorId()), message.getPhotos().toString(), message.authorId());
                    }
                    else {
                        res = quests.quest(authors.getParam(message.authorId()), message.getText(), message.authorId());
                    }

                    if ((param == 0)&&(authors.getSticker(authorId)!=0))
                    {
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .text("Ты уже получил стикеры. Но ты можешь пройти квест сначала просто так.")
                                .send();
                    }

                    if((param == 2)&&(res.get(0).equals("true"))&&(quests.questionBase.getNumber(Long.parseLong(message.authorId().toString()), 0) == 1))
                    {

                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .photo("source/27823198.jpg")
                                .send();
                    }

                    if((param == 2)&&(res.get(0).equals("true"))&&(quests.questionBase.getNumber(Long.parseLong(message.authorId().toString()), 0) != 1))
                    {

                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .photo(sources.getPhoto(1))
                                .send();
                    }

                    if ((param == 7)&&(res.get(0).equals("true")))
                    {
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .photo(sources.getPhoto(2))
                                .send();
                    }

                    if (((param == 2)||((param>2)&&(param<10)&&(authors.getHp(message.authorId())==1)))&&(res.get(0).equals("false")))
                    {
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .photo(sources.getPhoto(0))
                                .send();


                    }
                    if (((param == 1)||(param == 2)||(param == 7)||(param == 8)||(param == 9))&&(res.get(0).equals("true")))
                    {
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .sendVoiceMessage(sources.getVoice(param));
                    }

                    if ((res.get(0).equals("false"))&&((param > 1)&&(param < 10)))
                    {
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .sendVoiceMessage(sources.getVoice(0));

                    }





                    new Message()
                            .from(group)
                            .to(message.authorId())
                            .text(res.get(1))
                            .send();

                    if ((param == 8) && (res.get(0).equals("true")))
                        {
                                 new Message()
                                         .from(group)
                                         .to(message.authorId())
                                         .photo("source/Primer.jpg")
                                         .send();
                        }

                    if ((param == 9)&&(res.get(0).equals("true"))&&(authors.getSticker(authorId) != 0))
                    {
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .text("Спасибо, что прошел квест еще раз, но ты уже получил свои стикеры.")
                                .send();
                    }
                    if ((param == 9)&&(res.get(0).equals("true"))&&(authors.getSticker(authorId) == 0))
                    {
                        authors.putAuthor(message.authorId(), authors.getParam(message.authorId()), authors.getHp(message.authorId()), authors.getAdm(message.authorId()), 1);
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .text("Вот твои стикеры.")
                                .photo(sources.getSticker())
                                .send();
                    }

                    if((param == 2)&&(res.get(0).equals("true")))
                    {

                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .text("Для ответа на вопросы теста напиши цифру от 1 до 3.")
                                .send();
                    }

                    if ((res.get(0).equals("false"))&&((param > 2)&&(param < 10)))
                    {
                        int tmp = authors.getHp(authorId) - 1;
                        if (tmp != 0)
                        new Message()
                                .from(group)
                                .to(message.authorId())
                                .text("У тебя осталось попыток: " + tmp)
                                .send();
                    }
                    if ((res.get(0).equals("true"))&&((param == 8)))
                    {
                        int tmp = authors.getHp(authorId) - 1;
                        if (tmp != 0)
                            new Message()
                                    .from(group)
                                    .to(message.authorId())
                                    .text("У тебя есть 3 попытки.")
                                    .send();
                    }
                    if (!res.get(0).equalsIgnoreCase("false")) {
                        int hp;
                        if((param == 7)||(param == 8))
                        {
                            hp = 3;
                        }
                        else
                            {
                            hp = authors.getHp(authorId);
                        }
                        param++;
                        authors.putAuthor(authorId, param, hp , authors.getAdm(authorId), authors.getSticker(authorId));
                    } else if ((param > 2) && (param < 10)) {
                        int hp = authors.getHp(authorId);
                        hp--;
                        authors.putAuthor(authorId, authors.getParam(authorId), hp, authors.getAdm(authorId), authors.getSticker(authorId));
                    } else if (param == 2) {
                        flag = true;
                        authors.putAuthor(authorId, authors.getParam(authorId), 0, authors.getAdm(authorId), authors.getSticker(authorId));
                    }
                    if (authors.getHp(authorId) == 0) {
                        ban.setBan(message.authorId(), new Date(new Date().getTime() + 3600000));
                        authors.putAuthor(authorId, 0, 3, authors.getAdm(authorId), authors.getSticker(authorId));
                        if (flag == false)
                            new Message()
                                    .from(group)
                                    .to(message.authorId())
                                    .text("Ты ошибся 3 раза и проиграл.")
                                    .send();

                    }
                }
            }
        System.gc();
        }



        );
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new TimerTask(authors, ban, group), 0, 5, TimeUnit.SECONDS);


}
}