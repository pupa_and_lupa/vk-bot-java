package ru.service;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.URI;
/**
 * Подключение нейросети для обработки изображений в соответствии с документацией источника.
 * @author Microsoft
 */
public class VisioService {
    private static final String uriBase = "https://westeurope.api.cognitive.microsoft.com/vision/v2.0/ocr";
    private static final String uriBase1 = "https://westeurope.api.cognitive.microsoft.com/vision/v2.0/analyze";

    private static final String subscriptionKey = "aec2226b0543495cb1d3bb334947c267";
    private double sim, simmax;
    private String etalon = "МИРЭА РОССИЙСКИЙ ТЕХНОЛОГИЧЕСКИЙ УНИВЕРСИТЕТ";
    boolean visioService(String imageToAnalyze) {

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {

            URIBuilder uriBuilder = new URIBuilder(uriBase);

            uriBuilder.setParameter("language", "ru");
            uriBuilder.setParameter("detectOrientation", "true");

            // Request parameters.
            URI uri = uriBuilder.build();
            HttpPost request = new HttpPost(uri);

            // Request headers.
            request.setHeader("Content-Type", "application/json");
            request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

            // Request body.
            StringEntity requestEntity =
                    new StringEntity("{\"url\":\"" + imageToAnalyze + "\"}");
            request.setEntity(requestEntity);
            System.out.println(requestEntity);
            // Call the REST API method and get the response entity.
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            try {
            if (entity != null) {
                // Format and display the JSON response.
                String jsonString = EntityUtils.toString(entity);
                JSONObject json = new JSONObject(jsonString);
                System.out.println("REST Response:\n");
                System.out.println(json.toString());
                String[] tmp = json.toString().split("\"");
                String sout = "";
                for (int i = 0; i < tmp.length; i++) {
                    if (tmp[i].matches("(.*)[А-Яа-я](.*)"))
                        sout = sout + tmp[i] + " ";
                }
                    while (true){
                        sim = new Similary().similarity(sout, etalon.toUpperCase());
                        if (sim > 0.9) {
                            return true;
                        }
                        if (sim < simmax)
                        {
                            break;
                        }
                        else simmax = sim;
                        if (sout.contains(" ")) {
                            sout = sout.split(" ", 2)[1];
                        }
                        else break;

                    }
            }
            }catch (ArrayIndexOutOfBoundsException e) {return false;}
        } catch (Exception e) {
            // Display error message.
            //     System.out.println(e.getMessage());
        }
       return false;
    }

    boolean visioServiceTag(String imageToAnalyze) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {
            URIBuilder builder = new URIBuilder(uriBase1);

            // Request parameters. All of them are optional.
            builder.setParameter("visualFeatures", "Categories,Description,Color");
            builder.setParameter("language", "en");

            // Prepare the URI for the REST API method.
            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);

            // Request headers.
            request.setHeader("Content-Type", "application/json");
            request.setHeader("Ocp-Apim-Subscription-Key", subscriptionKey);

            // Request body.
            StringEntity requestEntity =
                    new StringEntity("{\"url\":\"" + imageToAnalyze + "\"}");
            request.setEntity(requestEntity);

            // Call the REST API method and get the response entity.
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            try {
                if (entity != null) {
                    // Format and display the JSON response.
                    String jsonString = EntityUtils.toString(entity);
                    JSONObject json = new JSONObject(jsonString);
                    System.out.println("REST Response:\n");
                    System.out.println(json.toString(2));
                    if ((json.toString().contains("posing"))&&(json.toString().contains("camera"))) {
                        return true;
                    }
                }
            }catch (ArrayIndexOutOfBoundsException e){return false;}
        } catch (Exception e) {
            // Display error message.
            System.out.println(e.getMessage());
        }
        return false;
    }
}