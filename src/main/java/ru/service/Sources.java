package ru.service;

import java.util.ArrayList;
import java.util.Random;
/**
 * Файлы, используемые в проекте.
 * @author А. Мягков
 * @version 1.0
 */
public class Sources {
    ArrayList<String> voice = new ArrayList<>();
    ArrayList<String> sticker = new ArrayList<>();
    ArrayList<String> photo = new ArrayList<>();
    final  int maxtrue = 11;
    final int maxfalse = 10;
    final int countsticker = 5;
    final int countphoto1 = 3; // количество фото, которые попадаются после 1 вороса
    final int countphoto2  = 3; // после квест теста
    final int countphoto3 = 4; // после бана;
    /**
     * Конструктор, прикрепляющий адреса файлов к проекту.
     */
    Sources()
    {
        voice.add(0,"source/90dec3ab74.mp3" );

        //true

        voice.add(1, "source/true/1.mp3");
        voice.add(2, "source/true/2.mp3");
        voice.add(3, "source/true/3.mp3");
        voice.add(4, "source/true/4.mp3");
        voice.add(5, "source/true/5.mp3");
        voice.add(6, "source/true/6.mp3");
        voice.add(7, "source/true/7.mp3");
        voice.add(8, "source/true/8.mp3");
        voice.add(9, "source/true/9.mp3");
        voice.add(10, "source/true/10.mp3");
        voice.add(11, "source/true/11.mp3");
        //false

        voice.add(12, "source/false/1.mp3");
        voice.add(13, "source/false/2.mp3");
        voice.add(14, "source/false/3.mp3");
        voice.add(15, "source/false/4.mp3");
        voice.add(16, "source/false/5.mp3");
        voice.add(17, "source/false/6.mp3");
        voice.add(18, "source/false/7.mp3");
        voice.add(19, "source/false/8.mp3");
        voice.add(20, "source/false/9.mp3");
        voice.add(21, "source/false/10.mp3");

        sticker.add(0, "source/sticker/1.jpeg");
        sticker.add(1, "source/sticker/2.jpeg");
        sticker.add(2, "source/sticker/3.jpeg");
        sticker.add(3, "source/sticker/4.jpeg");
        sticker.add(4, "source/sticker/5.jpeg");


        photo.add(0, "source/photo/true/1.png");
        photo.add(1, "source/photo/true/2.jpeg");
        photo.add(2, "source/photo/true/3.jpeg");
        photo.add(3, "source/photo/true/4.jpeg");
        photo.add(4, "source/photo/true/5.png");
        photo.add(5, "source/photo/true/6.jpeg");

        photo.add(6, "source/photo/ban/1.jpeg");
        photo.add(7, "source/photo/ban/2.jpeg");
        photo.add(8, "source/photo/ban/3.jpeg");
        photo.add(9, "source/photo/ban/4.jpeg");
    }
    /**
     * Получение звукового файла по номеру.
     * @param param - номер аудиозаписи.
     */
    String getVoice(int param)
    {
        if (param == 1)
        {
            return voice.get(0);
        }
        else
        {
            int voiceNumber;
            Random r = new Random(System.currentTimeMillis());
            if(param != 0)
                voiceNumber = (r.nextInt() % maxtrue);
            else
                voiceNumber = (r.nextInt() % maxfalse);
            if (voiceNumber<0)
                voiceNumber = 0 - voiceNumber;
            if (param!=0)
                voiceNumber = 1 + voiceNumber;
            else voiceNumber = 1 + maxtrue + voiceNumber;
            return  voice.get(voiceNumber);
        }
    }
    /**
     * Получение картинки со стикерами по номеру.
     * @param param - номер картинки.
     */
    String getSticker()
    {
    int stickerNumber;
    Random r = new Random(System.currentTimeMillis());
    stickerNumber =(r.nextInt()%countsticker);
    if(stickerNumber < 0) stickerNumber = 0 - stickerNumber;
    return sticker.get(stickerNumber);
    }
    /**
     * Получение картинки по номеру.
     * @param param - номер картинки.
     */
    String getPhoto(int param)
    {
           int photoNumber;
            Random r = new Random(System.currentTimeMillis());
            if (param == 0) {
                photoNumber = (r.nextInt() % countphoto3);
            }
            else if (param == 1)
            {
                photoNumber = (r.nextInt() % countphoto1);
            }
            else
                {
                    photoNumber = (r.nextInt() % countphoto2);
                }
            if (photoNumber < 0) photoNumber = 0 - photoNumber;

            if (param == 0)
            {
                photoNumber = photoNumber + countphoto1 + countphoto2;
            }
            else if(param != 1)
            {
                photoNumber = photoNumber + countphoto1;
            }
            return photo.get(photoNumber);

    }
}
