package ru.service;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
/**
 * Сохранение данных перед выключением бота.
 * @author А. Мягков
 * @version 1.0
 */
public class Save {
    /**
     * Сохранение участников.
     * @param authors - класс участников.
     */
    void saveAuthors(Authors authors)
    {
        try {
            FileWriter writer = new FileWriter("authors.bin", false);
            for (Map.Entry<Long, ArrayList<Integer>> item: authors.authors.entrySet())
            {
                writer.write(item.getKey().toString() + " " + authors.authors.get(item.getKey()).get(0)+ " " + authors.authors.get(item.getKey()).get(1)+ " " + authors.authors.get(item.getKey()).get(2) + " "+ authors.authors.get(item.getKey()).get(3));
                writer.append("\n");
            }
            writer.flush();
            writer.close();
        }catch (IOException e){}
    }
    /**
     * Сохранение ответов пользователей.
     * @param questionBase - класс, содержащий базу ответов.
     */
    void saveQuest (QuestionBase questionBase)
    {
        try {
            FileWriter writer = new FileWriter("quests.bin", false);
            for (Map.Entry<Long, ArrayList<Integer>> item: questionBase.authorsQuests.entrySet())
            {
                writer.write(item.getKey().toString() + " " + questionBase.authorsQuests.get(item.getKey()).get(0)+ " " + questionBase.authorsQuests.get(item.getKey()).get(1)+ " " + questionBase.authorsQuests.get(item.getKey()).get(2)+ " " + questionBase.authorsQuests.get(item.getKey()).get(3)+ " " + questionBase.authorsQuests.get(item.getKey()).get(4)+ " " + questionBase.authorsQuests.get(item.getKey()).get(5));
                writer.append("\n");
            }
            writer.flush();
            writer.close();
        }catch (IOException e){}
    }
    /**
     * Сохранение банлиста.
     * @param ban - класс, содержащий банлист.
     */
    void  saveBan(Banned ban)
    {
        try {
            SimpleDateFormat format =  new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss a", Locale.ENGLISH);
            FileWriter writer = new FileWriter("ban.bin", false);
            for (Map.Entry<Long, Date> item: ban.banList.entrySet())
            {
                writer.write(item.getKey().toString() + " " + format.format(ban.banList.get(item.getKey())));
                writer.append("\n");
            }
            writer.flush();
            writer.close();
        }catch (IOException e){}


    }

}
