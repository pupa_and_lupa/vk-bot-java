package ru.service;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/**
 * Список всех участников, их статус в системе и адм. права.
 * @author А. Мягков
 * @version 1.0
 */
public class Authors {
    Map<Long, ArrayList<Integer>> authors = new HashMap<>();

    /**
     * Конструктор Authors. Загрузка сохраненной информации о пользователях.
     */
    Authors() {
        try {
            File file = new File("authors.bin");
            //создаем объект FileReader для объекта File
            FileReader fr = new FileReader(file);
            //создаем BufferedReader с существующего FileReader для построчного считывания
            BufferedReader reader = new BufferedReader(fr);
            // считаем сначала первую строку
            String line = reader.readLine();
            while (line != null) {

                String[] tmp;
                tmp = line.split(" ", 5);
                ArrayList<Integer> tmp1 = new ArrayList<>();
                tmp1.add(0, Integer.parseInt(tmp[1]));
                tmp1.add(1, Integer.parseInt(tmp[2]));
                tmp1.add(2, Integer.parseInt(tmp[3]));
                tmp1.add(3, Integer.parseInt(tmp[4]));
                authors.put(Long.parseLong(tmp[0]), tmp1);
                // считываем остальные строки в цикле
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Получение param участника.
     *
     * @param authorId - id участника.
     */
    int getParam(long authorId) {
        return authors.get(authorId).get(0);
    }

    /**
     * Получение hp участника.
     *
     * @param authorId - id участника.
     */
    int getHp(long authorId) {
        return authors.get(authorId).get(1);
    }

    /**
     * Получение adm участника.
     *
     * @param authorId - id участника.
     */
    int getAdm(long authorId) {
        return authors.get(authorId).get(2);
    }

    /**
     * Получение aticker участника.
     *
     * @param authorId - id участника.
     */
    int getSticker(long authorId) {
        return authors.get(authorId).get(3);
    }

    /**
     * Проверка на наличие участника в системе.
     *
     * @param authorId - id участника.
     */
    boolean getAuthor(long authorId) {
        return authors.containsKey(authorId);
    }

    /**
     * Добавление участника.
     *
     * @param authorId - id участника.
     * @param param    - param участника.
     * @param hp       - hp участника.
     * @param adm      - adm участника.
     * @param sticker  - sticker участника.
     */
    void putAuthor(long authorId, int param, int hp, int adm, int sticker) {
        ArrayList<Integer> author = new ArrayList<>();
        author.add(0, param);
        author.add(1, hp);
        author.add(2, adm);
        author.add(3, sticker);

        ArrayList<Integer> put = authors.put(authorId, author);


    }

}