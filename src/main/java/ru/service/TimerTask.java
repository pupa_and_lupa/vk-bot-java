package ru.service;


import com.petersamokhin.bots.sdk.clients.Group;
import com.petersamokhin.bots.sdk.objects.Message;

import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
/**
 * Класс таймера для автоматических действий.
 * @author А. Мягков
 * @version 1.0
 */
public class TimerTask implements Runnable {
    Authors authors;
    Banned banned;
    Group group;
    /**
     * Конструктор таймера.
     * @param authors - класс участников.
     * @param banned - класс банлиста.
     * @param group - класс группы.
     */
    TimerTask(Authors authors, Banned banned, Group group)
    {
        this.authors = authors;
        this.banned = banned;
        this.group = group;
    }
    /**
     * Запуск службы разбана.
     */
    @Override
    public void run()
    {
        banService();
    }
    /**
     * Служба автоматического разбана.
     */
    void banService()
    {
        if(!banned.isEmpty())
        {
            LinkedList<Long> deleteBan = new LinkedList<>();
            for (Map.Entry<Long, Date> item: banned.banList.entrySet())
            {
                if (new Date().after(item.getValue()))
                {
                    deleteBan.push(item.getKey());
                }
            }
            while(!deleteBan.isEmpty())
            {
                long tmp = deleteBan.pop();
                banned.deleteBan(tmp);
                new Message()
                        .from(group)
                        .to(Integer.parseInt(Long.toString(tmp)))
                        .text("Все мы совершаем ошибки, поэтому я прощаю тебя. Можешь пройти квест сначала.")
                        .send();
            }
        }
    }
}

