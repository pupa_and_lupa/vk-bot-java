package ru.service;
/**
 * Проверка на попадание отправленных координат в заданную местность.
 * @author А. Мягков
 * @version 1.0
 */
public class Geo {
    /**
     * Конструктор geo.
     * param s - координаты, полученные от сервиса.
     */
    String geo(String s) {

        try {
            String[] tmp = s.split("\"geo\":", 2);
            String[] tmp1 = tmp[1].split("\"");
            System.out.println(tmp1[1]);
            if ((tmp1[1].matches("8PVEf[1, 2, 3, _](.?)(.?)")) || (tmp1[1].matches("8PVFK[i, o, p, q](.?)(.?)")))
                return "true";
            else return "false";
        }catch (ArrayIndexOutOfBoundsException e) {return "false0";}
    }
}
