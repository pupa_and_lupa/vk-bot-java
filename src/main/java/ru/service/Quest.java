package ru.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * Анализ ответа пользователя и реакция на него.
 * @author А. Мягков
 * @version 1.0
 */
public class Quest {
    QuestionBase questionBase = new QuestionBase();
    Messages mess = new Messages();
    Quest()
    {
        questionBase.questsBase();
    }
    /**
     * Конструктор класса Quest.
     * @param param - номер, по которому определяется, на какой вопрос отвечает пользователь.
     * @param message - сообщение пользователя.
     * @param authorID - пользователь, чей ответ находится на обработке.
     */
    public ArrayList<String> quest(int param, String message, long authorID) {

        ArrayList<String> ret = new ArrayList<>();
        switch (param) {
            case 0:
                ret.add(0, "true");
                ret.add(1, mess.getText(0));
                return ret; //"Привет. Если ты хочешь получить стикеры РТУ, пиши \"Хочу стикеры\"";
            case 1:
                if (message.matches("(.*)[С, с][Т, т][И, и][К, к][Е, е][Р, р](.*)")) {
                    questionBase.randomNumberFirst(authorID);
                    ret.add(0, "true");
                    ret.add(1, mess.getText(1) + questionBase.randomQuest(authorID, 0) + "\nУ тебя есть всего 1 попытка.");
                    return ret;// "Не спеши. Сначала докажи, что ты учишься в РТУ. Для этого ответь на простой вопрос " + questionBase.randomQuest(authorID, 0);
                }
                else {
                    ret.add(0, "false");
                    ret.add(1, mess.getText(2));
                }return ret;//"Неверно. Нужно ввести \"Хочу стикеры\"";
            case 2:
                if (message.matches(questionBase.answerQuest(authorID, 0))){
                    questionBase.randomNumber(authorID, 1);
                    ret.add(0, "true");
                    ret.add(1, mess.getText(3) + questionBase.randomQuest(authorID, 1));
                    return ret;
                }
                else
                {
                    ret.add(0, "false");
                    ret.add(1, "Ну о каких стикерах может идти речь, если ты даже не ответил на самый простой вопрос! Пока!");
                }
                return ret;
            case 3:
                if (message.equalsIgnoreCase(questionBase.answerQuest(authorID, 1))) {
                    questionBase.randomNumber(authorID, 2);
                    ret.add(0, "true");
                    ret.add(1, mess.getText(4) + "Вопрос 2: \n" + questionBase.randomQuest(authorID, 2));
                    return ret;
                }
                else
                {
                    ret.add(0, "false");
                    ret.add(1, mess.getText(9));
                    return ret;
                }
            case 4:
                if (message.equalsIgnoreCase(questionBase.answerQuest(authorID, 2))) {
                    questionBase.randomNumber(authorID, 3);
                    ret.add(0, "true");
                    ret.add(1, mess.getText(5) + "Вопрос 3:\n" + questionBase.randomQuest(authorID, 3));
                    return ret;
                }
                else {
                    ret.add(0, "false");
                    ret.add(1,mess.getText(9));
                    return ret;
                }
            case 5:
                if (message.equalsIgnoreCase(questionBase.answerQuest(authorID, 3))) {
                    questionBase.randomNumber(authorID, 4);
                    ret.add(0, "true");
                    ret.add(1,mess.getText(4) + "Вопрос 4:\n" + questionBase.randomQuest(authorID, 4));
                    return ret;
                }
                else {
                    ret.add(0, "false");
                    ret.add(1,mess.getText(9));
                    return ret;
                }
            case 6:
                if (message.equalsIgnoreCase(questionBase.answerQuest(authorID, 4))) {
                    questionBase.randomNumber(authorID, 5);
                    ret.add(0, "true");
                    ret.add(1,mess.getText(6) + questionBase.randomQuest(authorID, 5));
                    return ret;
                }
                else {
                    ret.add(0, "false");
                    ret.add(1,mess.getText(9));
                    return ret;
                }
            case 7:
                if (message.equalsIgnoreCase(questionBase.answerQuest(authorID, 5))) {
                    ret.add(0, "true");
                    ret.add(1,mess.getText(7) + "\nУ тебя есть 3 попытки.");
                    return ret;
                }
                else {
                    ret.add(0, "false");
                    ret.add(1,mess.getText(9));
                    return ret;
                }
            case 8:
                String geo= new Geo().geo(message);
                if (geo.equalsIgnoreCase("true")) {
                ret.add(0, "true"); // Ответ функции определения геопозиции
                ret.add(1,mess.getText(8));
                return ret;
            }
            else {
                ret.add(0, "false");
                if (geo.equalsIgnoreCase("false"))
                ret.add(1,mess.getText(10));
                else ret.add(1, "Ты не прислал мне карту.");
                return ret;
            }
            case 9:
                String image = new DataParcer().dataParcing(message);
                if (image.equals("false"))
                {
                    ret.add(0, "false");
                    ret.add(1,"Ты не прислал мне фотографию.");
                    return ret;
                }
                else {
                    boolean tag = new VisioService().visioServiceTag(image);
                    boolean visio = new VisioService().visioService(image);

                    if ((tag == true)&&(visio == true)) {
                        ret.add(0, "true");
                        ret.add(1, "Поздравляю, ты прошел квест.");
                        return ret;
                    } else if (tag == false){
                        ret.add(0, "false");
                        ret.add(1, "Я не вижу на фотографии твоего лица.");
                        return ret;
                    }
                    else
                        {
                            ret.add(0, "false");
                            ret.add(1, "Я не вижу на фотографии таблички МИРЭА.");
                            return ret;
                        }
                }
        }
        ret.add(0, "false");
        ret.add(1, "Ты выполнил все задания. Чтобы пройти квест сначала, введи команду: !сначала.");
        return ret;//"Default";
    }
}
