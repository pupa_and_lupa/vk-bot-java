package ru.service;

import java.io.*;
import java.util.*;
/**
 * Вопросы и ответы для тестовой части бота.
 * @author А. Мягков
 * @version 1.0
 */
public class QuestionBase {
    final int quest1 = 3;
    final int quest2 = 13;
    int questNumber = 0;
    Map<Long, ArrayList<Integer>> authorsQuests = new HashMap<>();
    Map<Integer, String> quests = new HashMap<>();
    Map<Integer, String> answers = new HashMap<>();
    /**
     * Конструктор QuestionBase. Загрузка цепочек вопросов для пользователей.
     */
    QuestionBase()
    {
        try {
            File file = new File("quests.bin");
            //создаем объект FileReader для объекта File
            FileReader fr = new FileReader(file);
            //создаем BufferedReader с существующего FileReader для построчного считывания
            BufferedReader reader = new BufferedReader(fr);
            // считаем сначала первую строку
            String line = reader.readLine();
            while (line != null) {
                String[] tmp;
                tmp = line.split(" ", 7);
                ArrayList<Integer> tmp1 = new ArrayList<>();
                tmp1.add(0,  Integer.parseInt(tmp[1]));
                tmp1.add(1,  Integer.parseInt(tmp[2]));
                tmp1.add(2,  Integer.parseInt(tmp[3]));
                tmp1.add(3,  Integer.parseInt(tmp[4]));
                tmp1.add(4,  Integer.parseInt(tmp[5]));
                tmp1.add(5,  Integer.parseInt(tmp[6]));
                authorsQuests.put(Long.parseLong(tmp[0]), tmp1);
                // считываем остальные строки в цикле
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     * Получение номера первого вопроса для пользователя и запись истории теста.
     * @param authorId - id пользователя.
     */
    public void randomNumberFirst(long authorId)
    {
        ArrayList<Integer> tmp = new ArrayList<>();
        Random r = new Random(System.currentTimeMillis());
        questNumber = (r.nextInt()%quest1);
        if (questNumber<0) questNumber = 0 - questNumber;
        questNumber++;
        tmp.add(0, questNumber);
        for (int i = 1; i < 6; i++)
        {
            tmp.add(i, 0);
        }
        authorsQuests.put(authorId, tmp);

    }
    /**
     * Получение номера вопроса тестовой части для пользователя и запись истории теста.
     * @param authorId - id пользователя.
     */
    public void randomNumber(long authorId, int p)
    {
        int par;
        ArrayList<Integer> tmp = authorsQuests.get(authorId);
            do {
            par = 0;
            Random r = new Random(System.currentTimeMillis());
            questNumber = (r.nextInt() % quest2);
            if (questNumber < 0) questNumber = 0 - questNumber;
            questNumber = questNumber + quest1 + 1;
            for (int i=0; i<p; i++)
                if (tmp.get(i) == questNumber) par = 1;

        } while (par == 1);
            try {
                tmp.set(p, questNumber);
            }catch (IndexOutOfBoundsException e){
            tmp.add(p, questNumber);}
        authorsQuests.put(authorId, tmp);
        System.out.println(authorsQuests);
    }

    /**
     * Создание базы вопросов.
     */
    public void questsBase()
    {
    quests.put(1, "Назовите самого строгого преподавателя по физике?");
    quests.put(2, "Какой корпус в МИРЭА самый большой?");
    quests.put(3, "На какой улице расположен главный корпус МГУПИ?");
    quests.put(4, "Как расшифровывается ИВЦ?\n1) Информационно-вычислительный центр.\n2) Интеллектуальная вычислительная цепь.\n3) Игровой внутренний центр.");
    quests.put(5, "Какой преподаватель чаще всего цитируется в паблике \"Цитатник РТУ\"?\n1) Пыркин А.Ю.\n2) Кашкин Е.В.\n3) Гущина Е.Н.");
    quests.put(6, "Сколько корпусов в МИРЭА?\n1) 4.\n2) 5.\n3) 6.");
    quests.put(7, "В каком году был создан МИРЭА? \n1) 1946.\n2) 1947.\n3)1950.");
    quests.put(8, "Через какой корпус можно пройти в спорткомплекс МИРЭА? \n1) А.\n2) В.\n3) Д.");
    quests.put(9, "Какой из корпусов МИРЭА является административным? \n1) А.\n2) В.\n3) Д.");
    quests.put(10, "Что в МИРЭА получило название \"штаны\"? \n1) Лестница.\n2) Тесты по БЖД. \n3) Гардероб.");
    quests.put(11, "Сколько всего лекционных аудиторий в МИРЭА? \n1) 16.\n2) 18.\n3) 20.");
    quests.put(12, "Какому из кампусов РТУ присвоено имя Ломоносова? \n1) МИРЭА.\n2) МГУПИ.\n3) МИТХТ.");
    quests.put(13, "Где расположены МИРЭА и МИТХТ? \n1) Улица Пушкина. \n2) Проспект Вернадского.\n3) Улица Вернадского.");
    quests.put(14, "В каком корпусе МИРЭА проходят лабораторные работы по физике? \n1) А.\n2) Б.\n3) В.");
    quests.put(15, "Сколько гардеробов в МИРЭА. \n1) 1.\n2) 2.\n3) 3.");
    quests.put(16, "Сколько длится перерыв между 2 и 3 парой? \n1) 10 минут.\n2) 20 минут.\n3) 50 минут.");




    answers.put(1, "(.*)[П, п][Ы, ы][Р, р][К, к][И, и][Н, н](.*)");
    answers.put(2, "[A, a, А, а]");
    answers.put(3, "(.*)[С, с][Т, т][Р, р][О, о][М, м][Ы, ы][Н, н][К, к][А, а, Е, е](.*)");
    answers.put(4, "1");
    answers.put(5, "2");
    answers.put(6, "2");
    answers.put(7, "2");
    answers.put(8, "3");
    answers.put(9, "3");
    answers.put(10, "1");
    answers.put(11, "2");
    answers.put(12, "3");
    answers.put(13, "2");
    answers.put(14, "3");
    answers.put(15, "2");
    answers.put(16, "3");

    }
    /**
     * Получение вопроса по номеру.
     * @param authorId - id пользователя.
     * @param p - номер вопроса.
 */
    public String randomQuest(long authorId, int p)
    {
        return quests.get(authorsQuests.get(authorId).get(p));
    }
    /**
     * Получение ответа на вопрос по номеру.
     * @param authorId - id пользователя.
     * @param p - номер вопроса.
     */
    public String answerQuest(long authorId, int p)
    {

        return answers.get(authorsQuests.get(authorId).get(p));
    }
    /**
     * Посмотреть ответ пользователя на вопрос.
     * @param authorId - id пользователя.
     * @param p - номер вопроса.
     */
    public int getNumber(long authorId, int number)
    {
        return authorsQuests.get(authorId).get(number);
    }

}
