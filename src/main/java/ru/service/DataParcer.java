package ru.service;
/**
 * Распознавание входящего сообщения.
 * @author А. Мягков
 * @version 1.0
 */
public class DataParcer {
    /**
     * Обработка входящего сообщения.
     * @param message - входящее сообщение.
     * @return - фото, если оно присутствует в полученном объекте, или false в противном случае.
     */
    String dataParcing(String message) {
        String image = new String();
        int maxheight = 0;
        try {
            if (!message.equals("[]")) {

                String[] tmp = message.split("\"");
                for (int i = 0; i < tmp.length; i++) {
                    if (tmp[i].matches("photo_(.*)")) {
                        String[] tmp1 = tmp[i].split("_");
                        int height = Integer.parseInt(tmp1[1]);
                        if (height > maxheight) {
                            maxheight = height;
                            image = tmp[i + 2];
                        }
                    }
                }
                return image;
            }
            //  System.out.println(maxheight);
            return "false";
        }catch (ArrayIndexOutOfBoundsException e){return "false";}
    }
}
