package ru.service;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Список забаненных участников.
 * @author А. Мягков
 * @version 1.0
 */
public class Banned{
    Map<Long, Date> banList = new HashMap<>();
    /**
     * Конструктор класса Banned. Загрузка банлиста из сохраненного файла.
     */
    Banned()
    {
        SimpleDateFormat format =  new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss a", Locale.ENGLISH);
        try {
            File file = new File("ban.bin");
            //создаем объект FileReader для объекта File
            FileReader fr = new FileReader(file);
            //создаем BufferedReader с существующего FileReader для построчного считывания
            BufferedReader reader = new BufferedReader(fr);
            // считаем сначала первую строку
            String line = reader.readLine();
            while (line != null) {
                try {
                    String[] tmp;
                    tmp = line.split(" ", 2);
                    Date date = (Date) format.parse(tmp[1]);
                    banList.put(Long.parseLong(tmp[0]), date);
                }catch (ParseException e){}

                // считываем остальные строки в цикле
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     * Провернка участника на наличие в банлисте.
     * @param authorId - id участника.
     */
    boolean getBanned(long authorId)
    {
        if (banList.containsKey(authorId)) return true;
        else return false;
    }
    /**
     * Получение времени окончания бана.
     * @param authorId - id участника.
     */
    Date getTime(long authorId)
    {
        return banList.get(authorId);
    }
    /**
     * Блокировка пользователя.
     * @param authorId - id участника.
     * @param date - срок блокировки.
     */
    void setBan(long authorId, Date date)
    {
        banList.put(authorId, date);
    }
    /**
     * Снятие бана.
     * @param authorId - id участника.
     */
    void deleteBan(long authorId)
    {
        banList.remove(authorId);
    }
    /**
     * Провернка банлиста на пустоту.
     */
    boolean isEmpty()
    {

        return  banList.isEmpty();
    }
}
